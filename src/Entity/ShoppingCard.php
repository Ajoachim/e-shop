<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCardRepository")
 */
class ShoppingCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $commande;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shoppingCard", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineShoppingBag", mappedBy="shoppingBag")
     */
    private $lineShoppingBags;

    public function __construct()
    {
        $this->lineShoppingBags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCommande(): ?bool
    {
        return $this->commande;
    }

    public function setCommande(bool $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|LineShoppingBag[]
     */
    public function getLineShoppingBags(): Collection
    {
        return $this->lineShoppingBags;
    }

    public function addLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if (!$this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags[] = $lineShoppingBag;
            $lineShoppingBag->setShoppingBag($this);
        }

        return $this;
    }

    public function removeLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if ($this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags->removeElement($lineShoppingBag);
            // set the owning side to null (unless already changed)
            if ($lineShoppingBag->getShoppingBag() === $this) {
                $lineShoppingBag->setShoppingBag(null);
            }
        }

        return $this;
    }
}
