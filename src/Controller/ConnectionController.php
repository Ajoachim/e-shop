<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\ConnectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\ProductRepository;

class ConnectionController extends Controller
{
    /**
     * @Route("/connection", name="connection")
     */
    public function index(AuthenticationUtils $utils, ProductRepository $repo) { 
    
        $error =$utils->getLastAuthenticationError();

        $lastEmail = $utils->getLastUsername();

        $products = $repo->findAll();

        
        return $this->render('connection/index.html.twig', [
            "products" => $products,
            'error' => $error,
            'lastEmail' => $lastEmail
        ]);
    }
}
